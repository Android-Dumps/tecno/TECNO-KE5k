#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:704bb0d5f6141173bd037ececa434da74f242698; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:8019ad10b835e58d0b57d76bcf90cb76a9506468 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:704bb0d5f6141173bd037ececa434da74f242698 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
