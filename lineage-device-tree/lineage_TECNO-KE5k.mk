#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-KE5k device
$(call inherit-product, device/tecno/TECNO-KE5k/device.mk)

PRODUCT_DEVICE := TECNO-KE5k
PRODUCT_NAME := lineage_TECNO-KE5k
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO KE5k
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-tecno

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_ke5k_h6122-user 10 QP1A.190711.020 58917 release-keys"

BUILD_FINGERPRINT := TECNO/KE5k-OP/TECNO-KE5k:10/QP1A.190711.020/CD-OP-201216V149:user/release-keys
