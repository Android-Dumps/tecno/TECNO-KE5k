#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-KE5k.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-KE5k-user \
    lineage_TECNO-KE5k-userdebug \
    lineage_TECNO-KE5k-eng
